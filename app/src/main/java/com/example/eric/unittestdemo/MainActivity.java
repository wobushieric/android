package com.example.eric.unittestdemo;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    private EditText txtPin;
    private Button btnLogin;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtPin = (EditText) findViewById(R.id.txPin);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        progress = (ProgressBar) findViewById(R.id.login_progress);
    }

    public void attemptLogin(View view) {
        boolean cancel = false;
        final String pin = txtPin.getText().toString();

        if (TextUtils.isEmpty(pin)) {
            txtPin.setError(getString(R.string.error_field_required));
            cancel = true;
        }

        //Right now it just fakes a 2 second delay
        if (!cancel) {
            progress.setVisibility(View.VISIBLE);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    progress.setVisibility(View.GONE);
                    if(isPinValid(pin)) {
                        Intent i = new Intent();
                        i.setClass(MainActivity.this.getApplicationContext(), OtherActivity.class);
                        startActivity(i);
                    } else {
                        txtPin.setError(getString(R.string.error_invalid_pin));
                    }
                }
            }, 2000);

        }
    }

    private boolean isPinValid(String pin) {
        return pin.equals("1234");
    }
}
