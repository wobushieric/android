package com.example.eric.unittestdemo;

import android.app.Instrumentation;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Eric on 2017-10-26.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    private static final String EMPTY_STRING = "";
    private String loginErrorMessage;

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void initValidString(){
        loginErrorMessage = mainActivityTestRule.getActivity().getString(R.string.error_field_required);
    }

    @Test
    public void faildLoginTest(){

        onView(withId(R.id.txPin)).perform(typeText(EMPTY_STRING), closeSoftKeyboard());

        onView(withId(R.id.btnLogin)).perform(click());

        onView(withId(R.id.txPin)).check(matches(hasErrorText(loginErrorMessage)));
    }

    @Test
    public void successLoginTest(){

        Instrumentation.ActivityMonitor otherActivityMonitor = getInstrumentation().addMonitor(OtherActivity.class.getName(), null, false);

        onView(withId(R.id.txPin)).perform(typeText("1234"), closeSoftKeyboard());

        onView(withId(R.id.btnLogin)).perform(click());

        View loginProgressView = mainActivityTestRule.getActivity().findViewById(R.id.login_progress);

        assertEquals(loginProgressView.getVisibility(), View.VISIBLE);

        OtherActivity otherActivity = (OtherActivity) otherActivityMonitor.waitForActivity();

        assertNotNull("Target Activity is not launched", otherActivity);
    }

}
